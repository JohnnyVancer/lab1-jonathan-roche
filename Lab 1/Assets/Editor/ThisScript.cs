﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ThisScript : MonoBehaviour {

    [MenuItem("Tool Creation/Create Folder")]
    public static void CreateFolder()
    {
        
        AssetDatabase.CreateFolder("Assets", "Materials");
        System.IO.File.WriteAllText(Application.dataPath + "/Materials/folderStructure.txt", "This folder is for storing materials!");
        
        AssetDatabase.CreateFolder("Assets", "Textures");
        System.IO.File.WriteAllText(Application.dataPath + "/Textures/folderStructure.txt", "This folder is for storing textures!");

        AssetDatabase.CreateFolder("Assets", "Prefabs");
        System.IO.File.WriteAllText(Application.dataPath + "/Prefabs/folderStructure.txt", "This folder is for storing Prefabs!");

        AssetDatabase.CreateFolder("Assets", "Scripts");
        System.IO.File.WriteAllText(Application.dataPath + "/Scripts/folderStructure.txt", "This folder is for storing Scripts!");

        AssetDatabase.CreateFolder("Assets", "Scenes");
        System.IO.File.WriteAllText(Application.dataPath + "/Scenes/folderStructure.txt", "This folder is for storing Scenes!");

        AssetDatabase.CreateFolder("Assets", "Animations");
        System.IO.File.WriteAllText(Application.dataPath + "/Animations/folderStructure.txt", "This folder is for storing raw Animations!");

        AssetDatabase.CreateFolder("Assets/Animations", "AnimationControllers");
        System.IO.File.WriteAllText(Application.dataPath + "/Animations/AnimationControllers/folderStructure.txt", "This folder is for storing Animation Controllers!");

        AssetDatabase.Refresh();
    }
}
